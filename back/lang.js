import Mustache from 'mustache';
import { marked } from 'marked';
import { readFile } from 'fs/promises';

export const translations = JSON.parse(
  await readFile(
    new URL('../translations.json', import.meta.url)
  )
);

export function parse (value, extra = {}) {
    if (typeof value === 'string') {
        return Mustache.render(value, extra);
    } else if ("length" in value) {
        return marked.parse(Mustache.render(value.join("\n"), extra));
    }
}
