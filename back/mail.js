import http from 'http';
import Mustache from 'mustache';
import { read } from './file.js';

const mustache = './back/templates/mail.mustache';
const from = process.env.POSTMARK_FROM || 'john.doe@example.com';
const options = {
    hostname: 'api.postmarkapp.com',
    port: 80,
    path: '/email',
    method: 'POST',
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'X-Postmark-Server-Token': process.env.POSTMARK_TOKEN || 'server token'
    }
};

export async function mail (to, subject, text) {
    try {
        const template = await read(mustache);
        const output = Mustache.render(template, { text: text });
        const post = {
            "From": from,
            "To": to,
            "Subject": subject,
            "TextBody": output.replace(/(<([^>]+)>)/gi, ""),
            "HtmlBody": "<html><body>" + output + "</body></html>",
            "MessageStream": "outbound"
        }

        return new Promise((resolve, reject) => {
            const request = http.request(options, (response) => {
                const buffers = [];

                response.setEncoding('utf8');
                
                response.on('data', (chunk) => {
                    buffers.push(chunk);
                });

                response.on('end', () => {
                    const data = buffers.length > 0 ? JSON.parse(buffers.join('')) : {};

                    resolve({
                        status: response.statusCode,
                        headers: response.headers,
                        body: data
                    });
                });
            });
            
            request.on('error', (e) => {
                reject(e);
            });
            
            request.write(JSON.stringify(post));
            request.end();
        });
    } catch (error) {
        throw error;
    }
    
}
