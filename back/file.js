import fs from 'fs';

export function read (filename) {
    return new Promise((resolve, reject) => {
        fs.readFile(filename, 'utf8', (error, content) => {
            if (error) {
                if (error.code == 'ENOENT') {
                    reject(404);
                } else {
                    reject(500);
                }
            } else {
                resolve(content);
            }
        });
    });
}
