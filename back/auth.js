import jwt from 'jsonwebtoken';

export function authorize (request) {
    const header = request.headers['authorization'];
    const token = header && header.split(' ')[1] || null;

    return new Promise((resolve, reject) => {
        if (token === null) {
            reject(401);
        }

        jwt.verify(token, process.env.SECRET, (err, user) => {
            if (err) {
                reject(403);
            }
            resolve(user);
        });
    });
}

export function tokenize (user) {
    return jwt.sign(user, process.env.SECRET);
}
