import Database from 'better-sqlite3';

export default class Base {
    constructor (filename) {
      this.db = new Database(filename);

      this.db.exec('CREATE TABLE IF NOT EXISTS users (id integer primary key, email text, hash text, activation text, activated integer, recovery text)');
    }
    
    getUser (email) {
        return this.db.prepare('SELECT * FROM users WHERE email = ?').get(email);
    }

    getUserActivation (key) {
        return this.db.prepare('SELECT * FROM users WHERE activation = ?').get(key);
    }

    getUserRecovery (key) {
        return this.db.prepare('SELECT * FROM users WHERE recovery = ?').get(key);
    }

    getAllUsers () {
        return this.db.prepare('SELECT id, email, activated FROM users').all();
    }

    createUser (user) {
        this.db.prepare('INSERT INTO users (email, hash, activation, activated) VALUES (@email, @hash, @activation, @activated)').run(user);
    }

    activateUser (email) {
        this.db.prepare('UPDATE users SET activated = 1 WHERE email = ?').run(email);
    }

    deleteUser (email) {
        this.db.prepare('DELETE FROM users WHERE email = ?').run(email);
    }

    setUserRecovery (email, recovery) {
        this.db.prepare('UPDATE users SET recovery = ? WHERE email = ?').run(recovery, email);
    }

    updateUserHash (email, hash) {
        this.db.prepare('UPDATE users SET hash = ? WHERE email = ?').run(hash, email);
    }
}
