const HTTPS = [443, 3443];
const PORTS = [80, 443];

export function validateEmail (email) {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  
    return re.test(String(email).toLowerCase());
}

export function getHostName (host, port) {
    return (HTTPS.includes(port) ? 'https' : 'http') + '://' + host + (PORTS.includes(port) ? '' : ':' + port);
}
