# Identity and Access Management

Backend and frontend code for user management/identity management/identity and access management application challenge.

## Setup
To set up all nicely, ```.env``` file needs to be created to configure server and services:

### .env

Backend setup:
```
SECRET=secret_jwt_token
HOST=localhost
PORT=3000
FRONT=front/build
DATABASE=storage.db
POSTMARK_TOKEN=postmark_token_provided_by_service
POSTMARK_FROM=john.doe@example.com
NODE_ENV=development
```
Change ```NODE_ENV``` to ```production``` to disable loose CORS policy built to enable tunnel between server on port 3000 and client on port 3001.

Frontend setup:
```
NODE_ENV=development
BACKEND=http://localhost:3000
```
Change ```BACKEND``` url if server is hosted on different host or different port. Would be nice to align it with the backend ```.env``` settings.

Note: currently ```BACKEND``` variable is only applied in ```development``` environment. Application assumes that frontend is based on the same server to avoid any ```CORS``` troubles. Also for the convinience of this demonstration. Might be changed in future versions, stay tuned!

## Install

```
npm install
cd front
npm install
```

## Run

### Backend
Production code run with simple command:

```npm run start```

For continious development ```nodemon``` module helps with restarts:

```npm run dev```

### Frontend
Actually, before running backend, especially in production mode, would be nice to build the package.

```cd front```

And now just as easy as:

```npm run build```

Of course, development version could be run too: 

```npm run start ```

And it will run on ```3001``` port.

## API
The whole ```index.js``` is basically an API endpoint description stuffed with auhorization, database calls and whatnot etc that appear as one huge script.

### Method, man!
To make some kind of separation of concerns, requests are handeled by method first then by the url.

#### OPTIONS
Solely exist to enable ```CORS``` for development environment.

Note: Development environment sets ```Access-Control-Allow-Origin``` to ```*```.

#### GET
As front end scripts are based on the same server, this could get a little busy handling all the client parts.

```http
GET /activate/?key=9eec0a99170fe164bef618cc5fce60efa6b9cc4d4bebfd011688da17319f1332
```

Handles all the activations users receive to their emails after registration. Checks if the ```key``` is found in the database and activates the user if not already activated. Redirects user right after activation complete or displays an error.

Does not have any fancy front end for this feature as the client application is made bearbones no router pure single page application. Maybe next version?

```http
GET /users
Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNjM3MTA0MTUyfQ.F3Lk5Q7103FYRFruxL17c2hEnHnKlOJCN6Zo1VqNtZM
```

Checks if request is authorized and serves user list in JSON format.

```http
GET /
```

Being the server server as in serving files, it checks if file exist and forwards its contents with proper content type.

In other words: static files served here.

#### POST
Most interesting stuff is here, all the fancy token issuing and user data validations.

```http
POST /authorize
Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNjM3MTA0MTUyfQ.F3Lk5Q7103FYRFruxL17c2hEnHnKlOJCN6Zo1VqNtZM
```

As frontend client is loaded, it checks weather it has valid token in hands and sends for authorization.


Server checks the request for if all is good and respons with JSON verdict.

```json
{
    "authorized": true
}
```

On the event that token is expired, not valid or any other internal server error might occur, authorization request will be denied:

```json
{
    "authorized": true
}
```

Basically reads the ```authorization``` header, and validates a JWT ```token```.

```http
POST /login
```

Awaits for valid and not empty ```email```/```password``` pair and issues token if all is correct.

Note: Passwords are compared with stored hash using ```bcrypt```.

```json
{
    "success": true,
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNjM3MTA0MTUyfQ.F3Lk5Q7103FYRFruxL17c2hEnHnKlOJCN6Zo1VqNtZM"
}
```

Otherwise, error message is served:
```json
{
    "success": false,
    "error": "email not found or password incorrect"
}
```

Error messages are used in frontend as labels to make proper wording and translatons:
* ```email empty```
* ```password empty```
* ```email invalid```
* ```email not found or password```
* ```user inactive```
* ```unknown error```

In the event of invalid password, unknown email or other 

```http
POST /register
```

In the same manner as the the ```/login``` request, takes pair of ```email``` and ```passord``` and checks if email not registered, password lenght is proper and if all correct puts the data in the database and sends email to new user.

Note: Passwords are hashed with ```bcrypt``` and stored as such.

```json
{
    "success": true,
    "message": "registered"
}
```

If something not really going well server responds with an error message:
```json
{
    "success": false,
    "error": "email exist"
}
```

Other types of messages could be presented to the frontend client:
* ```email empty```
* ```password empty```
* ```password short```
* ```email invalid```
* ```email exist```
* ```unknown error```

The email that user receives after successeful registration contains an activation link to ```/activate/```. The ```key``` sent is unique and assigned to user database.

```http
POST /recover
```
Works as a two parter:
1. Sends email with recovery link
2. Check recovery link key and changes the password.

If no recovery key is provided it checks if email is valid:
```http
POST /recover
{
    "email": "john.doe@example.com"
}
```
Usual stuff, checking validity and sending response with the message to check the email, that is being sent with the recovery link:
```json
{
    "success": true,
    "message": "check email"
}
```

On unlucky event of:
* ```email empty```
* ```email invalid```
* ```email exist```
* ```unknown error```

An error respons is sent:
```json
{
    "success": false,
    "error": "email not found"
}
```

The validation link that user receives after filling recovery form successefuly leads to password change page. After password is chosen, request is sent with validation key that was in an email along with freshly typed password:

```json
{
    "password": "password",
    "recovery": "2698e24bce7f5723a7dd083f51faec5402bb9d13d5cdeb5071cb072fe519cf6d"
}
```

Ah, such a secure password is chosen!

Similarly as in registration rules apply to password lenght.

If all is good, responce suggest user to log in:
```json
{
    "success": true,
    "message": "login"
}
```

In case of:
* ```password empty```
* ```password short```
* ```link expired```
* or pesky ```unknown error```

Client should expect the rejection:
```json
{
    "success": false,
    "error": "link expired"
}
```

#### DELETE
Last but not least, a very handy request method.

```http
DELETE /users
Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNjM3MTA0MTUyfQ.F3Lk5Q7103FYRFruxL17c2hEnHnKlOJCN6Zo1VqNtZM

{
    "email": "john.doe@example.com"
}
```

On a sad event of user needs to remove its data from the database, server checks for authorization and if user still in the records.
```json
{
    "success": true,
    "message": "user deleted"
}
```

Very sad :(

Also an email is sent to the user with farewell words.

There are couple of things that can go wrong before the bitter moment:
* ```unathorized```
* ```email empty```
* ```email invalid```
* ```user not found```

That would be presented as:
```json
{
    "success": false,
    "error": "unathorized"
}
```

#### PUT, PATCH and others...
Not too much to say. Would be great to use ```PATCH``` for password update and ```PUT``` for registration as it seems logical. Future versions?

## Localization
To make language maintanence easier, translations for both front and back are separately placed in corresponding ```translations.json``` file.

Backend translations file is in the root ```/``` folder and frontend one hides under ```/front/localization/```.

Architecturaly it is easy to add new languages with adding ISO 639-1 two-letter code as an property:
```json
{
    "en": {},
    "et": {}
}
```
**WARNING!!!** For purposes of this demo, languages are hardcoded as constants in both back and front counterparts.

To make translations easy to maintain, both ```mustache``` and ```markdown``` are supported for entry.

Example:
```json
{
    "en": {
        "mail": {
            "registration": {
                "confirmation": {
                    "subject": "Registration confirmation",
                    "body": ["Welcome! To complete registration click to [{{{url}}}]({{{url}}})"]
                }
            }
        }
    }
}
```

Both front and back includes ```parse``` function to handle such translations. 

```javascript
const subject = parse(translations[lang].mail.registration.confirmation.subject);

const text = parse(translations[lang].mail.registration.confirmation.body, { url: `${hostname}/activate/?key=${key}`});
```

This ```parse``` function runs translations as templates for ```mustachejs``` and feeds data to replace variables. If entry is multilined then it goes through ```markdown``` parser and outputs ```html```:

```html
<p>Welcome! To complete registration click to <a href="https://localhost:3001/activate/?key=zgybyrdyzgybyrdy">https://localhost:3001/activate/?key=zgybyrdyzgybyrdy</a></p>
```

Note that multilined entries are possible with splitting lines in array, like:
```json
{
    "body": [
        "Hi on line one",
        "Welcome on line two",
        "Thanks on line three"
    ]
}
```

## Emails
Spamming like a pro!

As requested, all email sending done via postmarkapp.com API, so the server application requires an account and registered email to run.

All the necessary credentials should be placed inside the ```.env``` in the root. 

## Database
All data is saved using ```better-sqlite3``` module in ```storage.db``` file that would be created in root category when app first runs. Ignored by GIT.

## Thoughts, regrets, remarks

### Backend

As mentioned before, would be great to use full on CRUD mentality and HTTP request types like ```PUT``` and ```PATCH```. Also the ```POST /recover``` implementation could be separated in two parts.

Using vanilla nodejs and no framework is quite a challange on itself. Question remains if this makes the app faster or the implementation is not solid enough to handle loads. Wich leads to...

All in one fat script! Basically the whole mess is inside ```createServer``` callback function. No middleware as in ExpressJS, so every time the header should be set. Some optimization could still be done though. But... It works!

One of the more serious concerns is JWT tokens. Currently being generated with sercet phrase and not expiring ever. The goal is to use public/private certificates to crypt the tokens and reniew them on a regular basis to avoid security issues.

### Frontend

The most questionable implementation in my opinion is single page application without proper router that reflects in the URL (```window.location.href```) current state of the application. Being the web app it important to have option to hit refresh and stay on the same page as before.

Props drilling! Probably should have used some kind of state management something something or srored translations, authorization and current page status in the session/local storage. For the current page is even better just to read the URL and route it correctly, so yes, router would be nice here.

A mess with user details and user view... Total disaster. This part should be departing back to the drawing board to make better abstraction. Router might could have helped too.

Authorizartion and keeping everything in top component might not be ideal, also it rerenders 3 times before displaying information, not sure weather it is good idea either.

Obviosly visual part could have been better. More styling, animations, notification popovers, custom confirmation windows, embedded MIDI player, bells and, of course, whistles.

### Bothends
Localization is done half way in the sense of not being able to switch it 'on the fly'. Basically it is currently single language applicaton with very easy way to translate it to another language. Fortunately not too much effort is required to make it properly multilingual.

Separation between front and back is also one of the concerns that is not addressed properly in the present implementation. Ideally backend server is separated from frontend and maybe fractured to auth, database. Maybe. Good start would be just a separate API and client servers.

### Anyways
Overall it was a good challange to make a means towards proper user management application in samall amount of time. As always made some mistakes in the process but learned something new along the way.

Happy coding!
