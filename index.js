'use strict';

import 'dotenv/config';

import { createServer } from 'http';

import bcrypt from 'bcryptjs';

import Base from './back/base.js';

import { authorize, tokenize } from './back/auth.js';
import { getContentType } from './back/mime.js';
import { read } from './back/file.js';
import { validateEmail, getHostName } from './back/help.js';
import { mail } from './back/mail.js';
import URL from 'url';
import { createHash } from 'crypto';
import { parse, translations } from './back/lang.js'

import { hrtime } from 'process';

const host = process.env.HOST || 'localhost';
const port = process.env.PORT || 3000;
const front = process.env.FRONT || 'front/build';
const database = process.env.DATABASE || 'storage.db';
const environment = process.env.NODE_ENV || 'development';

const lang = 'en';

const hostname = getHostName(host, port);

const base = new Base(database);

const server = createServer(async (request, response) => {
  if (environment === 'development') {
    response.setHeader('Access-Control-Allow-Origin', '*');
    response.setHeader('Access-Control-Allow-Credentials', 'true');
    response.setHeader('Access-Control-Allow-Methods', 'GET, HEAD, OPTIONS, POST, PUT, DELETE');
    response.setHeader('Access-Control-Max-Age', 2592000);
    response.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Authorization, Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');
  }

  const buffers = [];

  for await (const chunk of request) {
    buffers.push(chunk);
  }

  const data = buffers.length > 0 ? JSON.parse(Buffer.concat(buffers).toString()) : {};

  switch (request.method) {
    case 'OPTIONS':
      response.statusCode = 200;
      response.end();

    case 'GET':
      if (request.url.startsWith('/activate/')) {
        const query = URL.parse(request.url, true).query;

        try {
          const user = base.getUserActivation(query.key);
          
          if (!user) {
            throw 'user not found';
          }

          if (user.activated) {
            throw 'user already activated';
          }

          base.activateUser(user.email);
        } catch (error) {
          response.statusCode = 200;
          response.end(error);
        }

        response.statusCode = 303;
        response.setHeader('Location', hostname);
        response.end();

        break;
      }

      switch (request.url) {
        case '/users':
          try {
            const autorized = await authorize(request);

            if (!autorized) {
              throw 401;
            }
            
            const rows = base.getAllUsers();

            response.statusCode = 200;
            response.setHeader('Content-Type', 'application/json');
            response.end(JSON.stringify(rows));
            
          } catch (error) {
            response.statusCode = error;
            response.end();
          }
    
          break;

        default:
          const filename = './' + front + (request.url.endsWith('/') ? request.url + 'index.html' : request.url);
          const contentType = getContentType(filename);
    
          try {
            const content = await read(filename);

            response.statusCode = 200;
            response.setHeader('Content-Type', contentType);
            response.end(content, 'utf-8');
          } catch (error) {
            response.statusCode = error;
            response.end();
          }
    
          break;
      }

      break;

    case 'POST':
      switch (request.url) {
        case '/authorize':
          try {
            const autorized = await authorize(request);

            if (!autorized) {
              throw 401;
            }

            response.statusCode = 200;
            response.setHeader('Content-Type', 'application/json');
            response.end(JSON.stringify({
              authorized: true
            }));
            
          } catch (error) {
            response.statusCode = 200;
            response.setHeader('Content-Type', 'application/json');
            response.end(JSON.stringify({
              authorized: false
            }));
          }

          break;

        case '/login':
          try {
            if (data.email === '') {
              throw 'email empty';
            }

            if (data.password === '') {
              throw 'password empty';
            }

            if (!validateEmail(data.email)) {
              throw 'email invalid';
            }

            const user = base.getUser(data.email);

            if (!user || !bcrypt.compareSync(data.password, user.hash)) {
              throw 'email not found or password incorrect';
            }

            if (user.activated === 0) {
              throw 'user inactive';
            }

            const token = tokenize({
              id: user.id
            });

            response.statusCode = 200;
            response.setHeader('Content-Type', 'application/json');
            response.end(JSON.stringify({
              success: true,
              token: token
            }));
          } catch (error) {
            response.statusCode = 200;
            response.setHeader('Content-Type', 'application/json');
            response.end(JSON.stringify({
              success: false,
              error: typeof error === 'string' ? error : 'unknown error'
            }));
          }

          break;

        case '/register':
          try {
            if (data.email === '') {
              throw 'email empty';
            }

            if (data.password === '') {
              throw 'password empty';
            }

            if (data.password.trim().length < 8) {
              throw 'password short';
            }

            if (!validateEmail(data.email)) {
              throw 'email invalid';
            }

            if (base.getUser(data.email)) {
              throw 'email exist';
            }

            const hash = bcrypt.hashSync(data.password.trim());
            const key = createHash('sha256').update(data.email + hrtime.bigint()).digest('hex');
            const user = {
              email: data.email,
              hash: hash,
              activation: key,
              activated: 0,
            }

            base.createUser(user);

            const subject = parse(translations[lang].mail.registration.confirmation.subject);
            const text = parse(translations[lang].mail.registration.confirmation.body, { url: `${hostname}/activate/?key=${key}`});

            await mail(user.email, subject, text);
            
            response.statusCode = 200;
            response.setHeader('Content-Type', 'application/json');
            response.end(JSON.stringify({
              success: true,
              message: 'registered'
            }));
          } catch (error) {
            response.statusCode = 200;
            response.setHeader('Content-Type', 'application/json');

            response.end(JSON.stringify({
              success: false,
              error: typeof error === 'string' ? error : 'unknown error'
            }));
          }

          break;

        case '/recover':
          try {
            switch (data.recovery) {
              case '':
                try {
                  if (data.email === '') {
                    throw 'email empty';
                  }
      
                  if (!validateEmail(data.email)) {
                    throw 'email invalid';
                  }
      
                  const user = base.getUser(data.email);
      
                  if (!user) {
                    throw 'email not found';
                  }

                  const recovery = createHash('sha256').update(data.email + hrtime.bigint()).digest('hex');
                  const subject = parse(translations[lang].mail.recovery.request.subject);
                  const text = parse(translations[lang].mail.recovery.request.body, { url: `${hostname}/?recover=${recovery}`});

                  base.setUserRecovery(user.email, recovery);
      
                  await mail(user.email, subject, text);

                  response.statusCode = 200;
                  response.setHeader('Content-Type', 'application/json');
                  response.end(JSON.stringify({
                    success: true,
                    message: 'check email'
                  }));

                } catch (error) {
                  throw error;
                }
    
                break;

              default:
                try {
                  if (data.password === '') {
                    throw 'password empty';
                  }
      
                  if (data.password.trim().length < 8) {
                    throw 'password short';
                  }
  
                  const user = base.getUserRecovery(data.recovery);

                  if (!user) {
                    throw 'link expired';
                  }

                  const hash = bcrypt.hashSync(data.password.trim());
                  
                  base.updateUserHash(user.email, hash);
                  base.activateUser(user.email);
                  base.setUserRecovery(user.email, '');

                  response.statusCode = 200;
                  response.setHeader('Content-Type', 'application/json');
                  response.end(JSON.stringify({
                    success: true,
                    message: 'login'
                  }));
                } catch (error) {
                  throw error;
                }

                break;
            }
          } catch (error) {
            response.statusCode = 200;
            response.setHeader('Content-Type', 'application/json');
            response.end(JSON.stringify({
              success: false,
              error: typeof error === 'string' ? error : 'unknown error'
            }));
          }

          break;
      }
    case 'DELETE':
      switch (request.url) {
        case '/users':
          try {
            const autorized = await authorize(request);

            if (!autorized) {
              throw 'unathorized';
            }

            if (data.email === '') {
              throw 'email empty';
            }

            if (!validateEmail(data.email)) {
              throw 'email invalid';
            }

            const user = base.getUser(data.email);

            if (!user) {
              throw 'user not found';
            }

            base.deleteUser(user.email);

            const subject = parse(translations[lang].mail.deletion.information.subject);
            const text = parse(translations[lang].mail.deletion.information.body, { url: hostname});

            await mail(user.email, subject, text);

            response.statusCode = 200;
            response.setHeader('Content-Type', 'application/json');
            response.end(JSON.stringify({
              success: true,
              message: 'user deleted'
            }));
            
          } catch (error) {
            response.statusCode = 200;
            response.setHeader('Content-Type', 'application/json');
            response.end(JSON.stringify({
              success: false,
              error
            }));
          }

          break;
      }

      break;
  }
});

server.listen(port);
