import React, { useState } from 'react';
import { RecoverProps, User } from '../types';
import { Helmet } from 'react-helmet-async';
import { recover } from '../api';

export function Recover (props: RecoverProps) {
    const url = new URL(window.location.href)
    const params = new URLSearchParams(url.search);
    const recovery = params.get('recover') || '';
    const [user, setUser] = useState<User>({
        email: '',
        password: '',
        recovery,
    });
    const [error, setError] = useState('');
    const [message, setMessage] = useState('');
    const submit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();

        recover(user).then(response => {
            if (response.success) {
                setUser({ ...user, recovery: '' })
                setError('');
                setMessage(props.translations.pages.recover.form.messages.info[response.message]);

                if (response.message === 'login') {
                    window.history.replaceState({}, props.translations.navigation.login, '/');
                }
            } else {
                setMessage('');
                setError(props.translations.pages.recover.form.messages.error[response.error]);
            }
        })
    }

    return <section>
        <Helmet>
            <title>{props.translations.pages.recover.title}</title>
        </Helmet>
        <h2>{props.translations.pages.recover.heading}</h2>
        {message !== '' ? (
            <>
                <div className="message">{message}</div>
            </>
        ) : (
            <>
                <div className="message -error">{error}</div>
                <form onSubmit={submit}>
                    {recovery ? (
                        <>
                            <input type="password" name="password" value={user.password} onChange={e => setUser({ ...user, password: e.target.value })} />
                            <input type="submit" name="submit" value={props.translations.pages.recover.form.set} />
                        </>
                    ) : (
                        <>
                            <input type="email" name="email" value={user.email} onChange={e => setUser({ ...user, email: e.target.value })} />
                            <input type="submit" name="submit" value={props.translations.pages.recover.form.submit} />
                        </>
                    )}
                 </form>
            </>
        )}
    </section>;
}