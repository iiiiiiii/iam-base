import React, { useState, useEffect } from 'react';
import { getUsers } from '../api';
import { UsersList, UserDetails } from '../components';
import { UsersProps, UserType } from '../types';
import { Helmet } from 'react-helmet-async';

export function Users (props: UsersProps) {
    const [users, setUsers] = useState<UserType[]>([]);
    const [id, setId] = useState(0);
    const back = (refresh: Boolean = false) => {
        if (refresh) {
            getUsers().then(users => setUsers(users));
        }
        setId(0);
    };

    useEffect(() => {
        getUsers().then(users => setUsers(users));
    }, []);

    return <>
        <Helmet>
            <title>{props.translations.pages.users.title}</title>
        </Helmet>
        {id ? <UserDetails user={users.find(user => user.id === id)} return={back} setAuth={props.setAuth} translations={props.translations} /> : users.length > 0 ? <UsersList users={users} setId={setId} translations={props.translations}/> : 'Loading...'}
    </>;
}
