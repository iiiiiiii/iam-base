import React, { useState } from 'react';
import { AddProps, User } from "../types";
import { Helmet } from 'react-helmet-async';
import { register } from '../api';

export function Add (props: AddProps) {
    const [user, setUser] = useState<User>({
        email: '',
        password: ''
    });
    const [error, setError] = useState('');
    const [message, setMessage] = useState('');
    const submit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();

        register(user).then(response => {
            if (response.success) {
                setUser({
                    email: '',
                    password: ''
                });
                setError('');
                setMessage(props.translations.pages.add.form.messages.info[response.message]);
            } else {
                setMessage('');
                setError(props.translations.pages.add.form.messages.error[response.error]);
            }
        })
    }

    return <section>
        <Helmet>
            <title>{props.translations.pages.add.title}</title>
        </Helmet>
        <h2>{props.translations.pages.add.heading}</h2>
        <div className="message">{message}</div>
        <div className="message -error">{error}</div>
        <form onSubmit={submit}>
            <input type="email" name="email" value={user.email} onChange={e => setUser({ ...user, email: e.target.value })} />
            <input type="password" name="password" value={user.password} onChange={e => setUser({ ...user, password: e.target.value })} />
            <input type="submit" name="submit" value={props.translations.pages.add.form.submit} />
        </form>
    </section>;
}