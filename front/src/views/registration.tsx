import React, { useState } from 'react';
import { RegistrationProps, User } from '../types';
import { Helmet } from 'react-helmet-async';
import { register } from '../api';

export function Registration (props: RegistrationProps) {
    const [user, setUser] = useState<User>({
        email: '',
        password: ''
    });
    const [message, setMessage] = useState('');
    const submit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();

        register(user).then(response => {
            if (response.success) {
                props.setView('login');
                setMessage('');
            } else {
                setMessage(props.translations.pages.registration.form.messages.error[response.error]);
            }
        })
    }

    return <section>
        <Helmet>
            <title>{props.translations.pages.registration.title}</title>
        </Helmet>
        <h2>{props.translations.pages.registration.heading}</h2>
        <div className="message -error">{message}</div>
        <form onSubmit={submit}>
            <input type="email" name="email" value={user.email} onChange={e => setUser({ ...user, email: e.target.value })} />
            <input type="password" name="password" value={user.password} onChange={e => setUser({ ...user, password: e.target.value })} />
            <input type="submit" name="submit" value={props.translations.pages.registration.form.submit} />
        </form>
    </section>;
}
