import React, { useState } from 'react';
import { User, LoginProps } from '../types';
import { login } from '../api'
import { Helmet } from 'react-helmet-async';

export function Login (props: LoginProps) {
    const [user, setUser] = useState<User>({
        email: '',
        password: ''
    });
    const [message, setMessage] = useState('');
    const submit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();

        login(user).then(response => {
            if (response.success) {
                sessionStorage.setItem('token', response.token);
                props.setAuth(true);
                setMessage('');
            } else {
                setMessage(props.translations.pages.login.form.messages.error[response.error]);
            }
        });
    }

    return <section>
        <Helmet>
            <title>{props.translations.pages.login.title}</title>
        </Helmet>
        <h2>{props.translations.pages.login.heading}</h2>
        <span className="message -error">{message}</span>
        <form onSubmit={submit}>
            <input type="email" name="email" value={user.email} onChange={e => setUser({ ...user, email: e.target.value })} />
            <input type="password" name="password" value={user.password} onChange={e => setUser({ ...user, password: e.target.value })} />
            <input type="submit" name="submit" value={props.translations.pages.login.form.submit} />
        </form>
    </section>;
}
