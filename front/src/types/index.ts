
export interface Localization {
    en: Translations;
    et: Translations;
}

export interface Translations {
    [key: string]: any
}

export type Language = keyof Localization;

export type UserType = {
    id: Number;
    email: String;
    activated: Number;
}

export type UserJWT = {
    id: Number
}

export type User = {
    email?: string;
    password?: string;
    recovery?: string;
}

export type LoginProps = {
    setAuth: Function;
    translations: Translations;
}; 

export type UserProps = {
    id: Number;
    email: String;
    activated?: Number;
    setId: Function;
    translations: Translations;
};

export type UsersProps = {
    setAuth: Function;
    translations: Translations;
};

export type UsersListProps = {
    users: Array<UserType>;
    setId: Function;
    translations: Translations;
};

export type AddProps = {
    translations: Translations;
    setView: Function;
}

export type RecoverProps = {
    translations: Translations;
    setView: Function;
}

export type RegistrationProps = {
    translations: Translations;
    setView: Function;
}

export type UserDetailsProps = {
    user: UserType | undefined;
    return: Function;
    setAuth: Function;
    translations: Translations;
};

export type ViewportProps = {
    view: String;
    setView: Function;
    auth: Boolean;
    setAuth: Function;
    lang: Language;
    setLang: Function;
    translations: Translations;
}; 

export type NavigationProps = {
    view: String;
    setView: Function;
    auth: Boolean;
    setAuth: Function;
    lang: Language;
    setLang: Function;
    translations: Translations;
};
