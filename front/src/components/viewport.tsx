import React from 'react';
import { Login, Registration, Recover, Users, Add } from '../views';
import { ViewportProps } from '../types';

export function Viewport (props: ViewportProps) {
  switch (props.auth) {
    case true:
      switch (props.view) {
        case 'add':
          return <Add translations={props.translations} setView={props.setView}  />

        case 'users':
        default:
          return <Users setAuth={props.setAuth} translations={props.translations} />;
      }

    case false:
    default:
      switch (props.view) {
        case 'recover':
          return <Recover translations={props.translations} setView={props.setView} />

        case 'registration':
          return <Registration translations={props.translations} setView={props.setView}  />

        case 'login':
        default:
          return <Login setAuth={props.setAuth} translations={props.translations} />;
      }
  }
}
