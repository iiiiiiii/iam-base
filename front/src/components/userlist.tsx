import React from 'react';
import { User } from '../components';
import { UsersListProps } from '../types';

export function UsersList (props: UsersListProps) {
    return <section>
        {props.users.map((user, index) => <User key={index} id={user.id} activated={user.activated} email={user.email} setId={props.setId} translations={props.translations} />)}
    </section>;
}