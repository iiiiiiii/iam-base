import React, { useState } from 'react';
import { UserDetailsProps, UserJWT } from '../types';
import { Helmet } from 'react-helmet-async';
import { remove, getToken, removeToken } from '../api';
import jwt from 'jwt-decode';


export function UserDetails (props: UserDetailsProps) {
    const user: UserJWT = jwt(getToken()); 
    const back = (event: React.MouseEvent<HTMLElement>) => {
        event.preventDefault();
        props.return(event);
    }
    const [message, setMessage] = useState('');
    const propmt = () => {
        if (confirm(props.translations.pages.userdetails.labels.confirm)) {
            if (props.user) {
                remove(props.user).then(response => {
                    if (response.success) {
                        if (props.user?.id === user.id) {
                            removeToken();
                            props.setAuth(false);
                        } else {
                            props.return(true);
                        }
                    } else {
                        setMessage(props.translations.pages.userdetails.form.messages.error[response.error]);
                    }
                });
            }
        }
    }

    return <section>
        <Helmet>
            <title>{props.translations.pages.userdetails.title}</title>
        </Helmet>
        <h2>{props.translations.pages.userdetails.heading}</h2>
        <p>{props.user?.email}{props.user?.id === user.id ? ' (' + props.translations.pages.userdetails.labels.you + ')' : ''} ({props.user?.activated ? props.translations.pages.userdetails.labels.activated : props.translations.pages.userdetails.labels.deactivated})</p>
        <p><button type="button" onClick={propmt}>{props.translations.pages.userdetails.form.delete}</button></p>
        <div className="message -error">{message}</div>
        <p><a href="?" onClick={back}>{props.translations.navigation.back}</a></p>
    </section>;
}