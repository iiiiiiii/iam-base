import React from 'react';
import { UserProps } from '../types';

export function User (props: UserProps) {
    const setId = (event: React.MouseEvent<HTMLElement>) => {
        event.preventDefault();
        props.setId(props.id)
    }

    return <div className={'user' + (props.activated ? '' : ' -inactive')}>
        <a href="?" onClick={setId}>{props.email}</a>
    </div>
}
