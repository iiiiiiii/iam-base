import React from 'react';
import { NavigationProps, Language, Localization } from '../types';
import { getLang } from '../modules/translation';
import { removeToken } from '../api';
import translations from '../../localization/translations.json';

export function Navigation (props: NavigationProps) {
    const lang: Language = getLang();
    const translated = (translations as Localization)[lang];
    const logout = () => {
        removeToken();
        props.setAuth(false);
    };

    return <nav>
        <ul>
        {props.auth ? (<>
            <li onClick={() => props.setView('users')}>{translated.navigation.users}</li>
            <li onClick={() => props.setView('add')}>{translated.navigation.add}</li>
            <li onClick={() => logout()}>{translated.navigation.logout}</li>
        </>) : (<>
            <li onClick={() => props.setView('login')}>{translated.navigation.login}</li>
            <li onClick={() => props.setView('registration')}>{translated.navigation.registration}</li>
            <li onClick={() => props.setView('recover')}>{translated.navigation.recover}</li>
        </>)}
        </ul>
    </nav>
}
