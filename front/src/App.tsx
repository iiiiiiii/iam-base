import React, { useState, useEffect } from 'react';
import { authorize } from './api';
import { Navigation, Viewport } from './components';
import { HelmetProvider } from 'react-helmet-async';
import { Language, Localization } from './types';
import { getLang } from './modules/translation';
import translations from '../localization/translations.json';

function App () {
  const url = new URL(window.location.href)
  const params = new URLSearchParams(url.search);
  const [lang, setLang] = useState<Language>(getLang());
  const [auth, setAuth] = useState(false);
  const [view, setView] = useState('login');
  const translated = (translations as Localization)[lang];

  if (params.get('recover') !== null && view !== 'recover') {
    setView('recover');
  }

  useEffect(() => {
      authorize().then(response => response.authorized ? [setAuth(true), setView('users')] : [setAuth(false), setView('login')]);
  }, []);

  return (
    <HelmetProvider>
      <Navigation auth={auth} setAuth={setAuth} view={view} setView={setView} lang={lang} setLang={setLang} translations={translated} />
      <Viewport auth={auth} setAuth={setAuth} view={view} setView={setView} lang={lang} setLang={setLang} translations={translated} />
    </HelmetProvider>
  );
}

export default App;
