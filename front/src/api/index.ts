import { User, UserType } from '../types';

export const getToken = () => sessionStorage.getItem('token') || '';
export const removeToken = () => sessionStorage.removeItem('token');

const backend = process.env.NODE_ENV !== 'production' ? process.env.DEV_BACKEND || '' : '';
const headers = {
    'Content-Type': 'application/json',
};
const authorized = () => ({
  ...headers,
  'Authorization': 'Bearer ' + getToken(),
});

export async function authorize () {
    const response = await fetch(backend + '/authorize', {
      method: 'POST',
      headers: authorized(),
    });
  
    return response.json();
}

export async function login (user: User) {
    const response = await fetch(backend + '/login', {
      method: 'POST',
      body: JSON.stringify(user),
      headers: headers,
    });
  
    return response.json();
}

export async function register (user: User) {
    const response = await fetch(backend + '/register', {
      method: 'POST',
      body: JSON.stringify(user),
      headers: headers,
    });
  
    return response.json();
}

export async function recover (user: User) {
    const response = await fetch(backend + '/recover', {
      method: 'POST',
      body: JSON.stringify(user),
      headers: headers,
    });
  
    return response.json();
}

export async function remove (user: UserType) {
    const response = await fetch(backend + '/users', {
      method: 'DELETE',
      body: JSON.stringify(user),
      headers: authorized(),
    });
  
    return response.json();
}

export async function getUsers () {
    const response = await fetch(backend + '/users', {
      method: 'GET',
      headers: authorized(),
    });
  
    return response.json();
}
