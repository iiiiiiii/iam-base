import Mustache from 'mustache';
import { marked } from 'marked';
import { Language } from '../types';

export const getLang = (): Language => 'en';

export const parse = (value: String | Array<String>, extra = {}): String => typeof value === 'string' ? Mustache.render(value, extra) : "length" in value ? marked.parse(Mustache.render((value as Array<String>).join("\n"), extra)) : '';
